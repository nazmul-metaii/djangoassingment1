# Django Assignment Part-1

## Details
This is a simple index view creating project. This index view is created using
django framework. The index view will return a one page website with one section.
The page will have one image that takes up the full size of the screen.



## version
```
python 3.6.8
```
## Starting Virtual Environment
```
pip install virtualenv
python3 -m virtualenv Env
```

## Directory
1. ```source/Env/bin/activate``` to run virtual environment .
2. Enter into the working directory where manage.py locate .

## Installing project dependency
```
pip3 install -r requirements.txt
```

## Run project
```
python manage.py runserver
```
